import configparser
import logging
import time
from random import choices

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler

from modules.extra.db_anime_parser import UpdateAnimeTitles
from modules.posting.coub import CoubPoster
from modules.posting.image import ImagePoster
from modules.posting.recommendation_anime import RecommendationPoster
from modules.posting.season_anime import SeasonPoster
from modules.posting.season_anime_anilist import AnilistSeasonPoster
from modules.status_set import text_status_update

config = configparser.ConfigParser()
config.read('config.ini')


class Scheduler:
    def __init__(self):
        self.scheduler = BlockingScheduler(job_defaults={'misfire_grace_time': 60})
        self.scheduler.replace_existing = True
        self.timer = config['BotSettings'].get("interval_timer")
        self.day_scheduler = False
        logging.info(f'\n__InfinitePostingBot start__\n'
                     f'Image directory: {config["BotSettings"].get("path")}\n'
                     f'Interval timer: {self.timer}')

    def set_interval(self):
        self.scheduler.add_job(lambda: [self.__switch(), image_post()], 'interval',
                               minutes=int(self.timer), max_instances=4, id='post_day', jitter=60)
        self.scheduler.add_job(lambda: [self.__switch(), image_post()], 'interval',
                               hours=1, max_instances=4, id='post_night', jitter=60)
        self.scheduler.pause_job('post_day')

    def start(self):
        self.__switch()
        self.scheduler.start()

    def _is_day(self):
        if 7 < int(time.strftime('%H')) < 20:
            return True
        else:
            return False

    def __switch(self):
        if not self.day_scheduler and self._is_day():
            logging.info('\n\npost_night PAUSE | post_day RESUME\n')
            self.scheduler.pause_job('post_night')
            self.scheduler.resume_job('post_day')
            self.day_scheduler = True
        elif self.day_scheduler and not self._is_day():
            logging.info('\n\npost_day PAUSE | post_night RESUME\n')
            self.scheduler.pause_job('post_day')
            self.scheduler.resume_job('post_night')
            self.day_scheduler = False


def background_tasks():
    back_scheduler = BackgroundScheduler(job_defaults={'misfire_grace_time': 60})
    back_scheduler.add_job(season_post, 'cron',
                           day_of_week=6, hour=17, max_instances=3, id='top_anime')
    back_scheduler.add_job(multiple_image_post, 'cron',
                           hour='5,7,10,13,16,18,19,20', max_instances=3, id='arts')
    back_scheduler.add_job(coub_post, 'cron',
                           hour='8,14,18', minute=30, day_of_week='mon-fri',
                           max_instances=3, id='coub')
    back_scheduler.add_job(gif_post, 'cron',
                           hour='8,14,18,20', minute=30, day_of_week='5-6', max_instances=3, id='gif_only')
    back_scheduler.add_job(recommendation_post, 'cron',
                           hour='15,17', day_of_week='mon-sat', max_instances=3, id='recommend_anime')
    back_scheduler.replace_existing = True
    return back_scheduler


def gif_post():
    logging.info(f'`gif post` START')
    try:
        gif_poster = ImagePoster(1, 'gif')
        result = gif_poster.post()
        if result:
            logging.info('`gif post` SUCCESS')
        else:
            logging.error('`gif post` ERROR')
    except Exception as e:
        logging.exception(f'`gif post` EXCEPTION: {e}')
    update_status()


def image_post():
    poster_type = choices(('yandex', 'local'), (5, 1))[0]
    logging.info(f'`{poster_type} image post` START')
    try:
        poster = ImagePoster(1, poster_type)
        result = poster.post()
        if result:
            logging.info(f'`{poster_type} image post` SUCCESS')
        else:
            logging.error(f'`{poster_type} image post` ERROR')
    except Exception as e:
        logging.exception(f'`{poster_type} image post` EXCEPTION: {e}')
    update_status()


def multiple_image_post():
    logging.info(f'`multiple image post` START')
    try:
        local_poster = ImagePoster(6, 'local')
        result = local_poster.post()
        if result:
            logging.info('`multiple image post` SUCCESS')
        else:
            logging.error('`multiple image post` ERROR')
    except Exception as e:
        logging.exception(f'`multiple image post` EXCEPTION: {e}')
    update_status()


def update_status():
    vk_token = config['VK'].get("token")
    group_id = config['VK'].get("group_id")
    ya_token = config['Yandex'].get('token')
    try:
        result = text_status_update(vk_token, ya_token, group_id)
        if result:
            logging.info('`status update` SUCCESS')
        else:
            logging.error('`status update` ERROR')
    except Exception as e:
        logging.exception(f'`status update` EXCEPTION: {e}')


def season_post():
    logging.info(f'`season_anime post` START')
    try:
        poster = SeasonPoster()
        result = poster.post()
        if result:
            logging.info('`season_anime post` SUCCESS')
        else:
            logging.error('`season_anime post` ERROR')
    except Exception as e:
        logging.exception(f'`season_anime post` EXCEPTION: {e}')
        season_anilist_post()
    u = UpdateAnimeTitles()
    u.update_db()


def season_anilist_post():
    logging.info(f'`season anilist post` START')
    try:
        poster = AnilistSeasonPoster()
        result = poster.post()
        if result:
            logging.info('`season anilist post` SUCCESS')
        else:
            logging.error('`season anilist post` ERROR')
    except Exception as e:
        logging.exception(f'`season anilist post` EXCEPTION: {e}')


def coub_post():
    logging.info(f'`coub post` START')
    try:
        poster = CoubPoster()
        result = poster.post()
        if result:
            logging.info('`coub post` SUCCESS')
        else:
            logging.error('`coub post` ERROR')
    except Exception as e:
        logging.exception(f'`coub post` EXCEPTION: {e}')


def recommendation_post():
    logging.info(f'`recommendation post` START')
    try:
        poster = RecommendationPoster()
        result = poster.post()
        if result:
            logging.info('`recommendation post` SUCCESS')
        else:
            logging.error('`recommendation post` ERROR')
    except Exception as e:
        logging.exception(f'`recommendation post` EXCEPTION: {e}')
