CREATE TABLE IF NOT EXISTS `recommendation_page`
(
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `anime_id` INTEGER,
  `page` INTEGER
);
CREATE TABLE IF NOT EXISTS `shikimori_token`
(
  `access_token` TEXT,
  `refresh_token` TEXT
);