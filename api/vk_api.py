import logging
import os
import time
from copy import deepcopy
from enum import Enum
from io import BytesIO

import requests
from PIL import Image

Image.MAX_IMAGE_PIXELS = None


class DocUploader:
    def __init__(self, token, group_id, doc):
        self.doc = doc
        self.params = {'access_token': token, 'group_id': group_id, 'v': '5.130'}

    def upload(self, name):
        if isinstance(self.doc, bytes):
            doc = self._upload_doc(self.doc, name)
        else:
            with open(self.doc, mode="rb") as data:
                content = data.read()
            doc = self._upload_doc(content, name)
        return doc

    def _upload_doc(self, content, name):
        params = self.params
        response = requests.post("https://api.vk.com/method/docs.getUploadServer", data=params).json()
        upload_url = response['response']['upload_url']
        form_data = {'file': (name, content)}
        response = requests.post(upload_url, files=form_data).json()
        params.update({"file": response['file']})
        params.pop("group_id")
        response = requests.post("https://api.vk.com/method/docs.save", data=params).json()
        return f"doc{response['response']['doc']['owner_id']}_{response['response']['doc']['id']}"


class ImageUploader:
    def __init__(self, token, group_id, image):
        self.image = image
        self.params = {'access_token': token, 'group_id': group_id, 'v': '5.130'}

    def upload(self, name):
        if isinstance(self.image, bytes):
            content = self.resize_image(BytesIO(self.image))
        else:
            image = self.resize_image(self.image)
            with open(image, mode="rb") as data:
                content = data.read()
        photo = self._upload_image(content, name)
        return photo

    def _upload_image(self, content, name):
        params = self.params
        response = requests.post("https://api.vk.com/method/photos.getWallUploadServer", data=params).json()
        upload_url = response['response']['upload_url']
        form_data = {'photo': (name, content)}
        response = requests.post(upload_url, files=form_data).json()
        params.update({"photo": response['photo'], "server": response['server'], 'hash': response['hash']})
        response = requests.post("https://api.vk.com/method/photos.saveWallPhoto", data=params).json()
        return f"photo{response['response'][0]['owner_id']}_{response['response'][0]['id']}"

    @staticmethod
    def resize_image(image):
        img = Image.open(deepcopy(image))
        if sum(img.size) > 12000:
            wpercent = (2000 / float(img.size[0]))
            hsize = int((float(img.size[1]) * float(wpercent)))
            img = img.resize((2000, hsize), Image.LANCZOS)
            img.save(image, format='jpeg')
        img.close()
        return image


class FileType(Enum):
    doc = 'doc'
    image = 'image'
    video = 'video'


class Attachment:
    def __init__(self, name, content, type, album_id=""):
        self.name = name
        self.content = content
        if type not in FileType.__members__.keys():
            raise Exception('Unsupported file type')
        self.type = FileType(type)
        self.album_id = album_id


class Posting:
    def __init__(self, token, group_id, message: str, attachments: list):
        self.token = token
        self.group_id = group_id
        self.params = {'access_token': token, 'v': '5.130'}
        self.message = message
        self.attachments = attachments

    def create_post(self, source=None):
        attachments = self.upload_attachments()
        logging.debug(attachments)

        params = self.params.copy()
        params.update({"owner_id": f"-{self.group_id}",
                       "from_group": 1, 'message': self.message,
                       'attachment': ','.join(attachments),
                       'copyright': str(source)})
        response = requests.post("https://api.vk.com/method/wall.post", data=params).json()
        post_id = response['response']['post_id']

        if attachments:
            params_wall_get = self.params.copy()
            params_wall_get.update({'posts': f"-{self.group_id}_{post_id}"})
            post = requests.post("https://api.vk.com/method/wall.getById", data=params_wall_get).json()

            for i in range(5):
                attachments_bug = (
                        ('attachments' not in post['response'][0])
                        or
                        (len(post['response'][0]['attachments']) < len(attachments))
                )
                if not attachments_bug:
                    break
                attachments = self.upload_attachments()
                logging.warning(
                    'attachments in post:%s, attachments uploaded: %s',
                    len(post['response'][0].get('attachments', [])), len(attachments)
                )
                logging.warning(f'attachments_bug:{attachments_bug} - i:{i}')
                message = post['response'][0]['text']

                params = self.params.copy()
                params.update({'owner_id': f"-{self.group_id}", 'post_id': post_id, 'message': message,
                               'attachments': ','.join(attachments), 'copyright': str(source)})
                requests.post("https://api.vk.com/method/wall.edit", data=params)

                post = requests.post("https://api.vk.com/method/wall.getById", data=params_wall_get).json()

            if 'attachments' in post['response'][0]:
                params = self.params.copy()
                edit_video = False

                for at in post['response'][0]['attachments']:
                    if at.get('type') == 'video':
                        logging.debug(f"video: {at['video']}")
                        if at['video']['duration'] == 0 and at['video']['title'].startswith('Видео от'):
                            try:
                                attachments.remove(f"video{at['video'].get('owner_id')}_{at['video'].get('id')}")
                                edit_video = True
                            except ValueError:
                                pass
                if edit_video and len(attachments) > 0:
                    params.update({"owner_id": f"-{self.group_id}", 'post_id': post_id,
                                   'message': self.message, 'attachment': ','.join(attachments),
                                   'copyright': str(source)})
                    response = requests.post("https://api.vk.com/method/wall.edit", data=params)
                    logging.debug(f'edit post with video: {response.text}')
                elif edit_video and not attachments:
                    response = requests.post("https://api.vk.com/method/wall.delete",
                                             data={**self.params,
                                                   "owner_id": f"-{self.group_id}", 'post_id': post_id})
                    logging.debug(f'delete post without attachments: {response.text}')
        return post_id

    def upload_attachments(self):
        attachments = []
        upload_status = {}
        for attach in self.attachments:
            upload_status[attach] = False
            if attach.type.value == 'image':
                image_uploader = ImageUploader(self.token, self.group_id, attach.content)
                attach = image_uploader.upload(attach.name)
            elif attach.type.value == 'doc':
                doc_uploader = DocUploader(self.token, self.group_id, attach.content)
                attach = doc_uploader.upload(attach.name)
            elif attach.type.value == 'video':
                attach = self.video_save(attach.album_id, attach.content)
            if attach:
                upload_status[attach] = True
                attachments.append(attach)
            time.sleep(0.8)
        time.sleep(5)
        return attachments

    def video_save(self, album_id, link):
        params = self.params.copy()
        params.update({"album_id": album_id, "owner_id": f"-{self.group_id}"})
        response = requests.post("https://api.vk.com/method/video.getAlbumById", data=params).json()
        count = response.get('response').get('count')
        if count >= 1000:
            raise Exception('Album is full')
        else:
            if 'vk.com' in link:
                video = os.path.split(link)[1].replace('video', '').split('_')  # улучшить используя регулярки, позже
                params.update({"target_id": f"-{self.group_id}", "album_id": album_id,
                               "owner_id": video[0], "video_id": video[1]})
                response = requests.post("https://api.vk.com/method/video.addToAlbum", data=params).json()
                if response['response'] == 1:
                    return f"video{video[0]}_{video[1]}"
                else:
                    return None
            else:
                params.update({"group_id": f"{self.group_id}", "album_id": album_id, "link": link})
                response = requests.post("https://api.vk.com/method/video.save", data=params).json()
                requests.get(response['response']['upload_url'])
                return f"video{response['response']['owner_id']}_{response['response']['video_id']}"


def wall_pin(token, post_id, owner_id):
    params = {'access_token': token, 'post_id': post_id, 'owner_id': owner_id, 'v': '5.130'}
    response = requests.post("https://api.vk.com/method/wall.pin", data=params).json()
    return response['response']


def status_update(token, group_id, text):
    params = {'access_token': token, 'group_id': group_id, 'text': text, 'v': '5.130'}
    response = requests.post("https://api.vk.com/method/status.set", data=params).json()
    return response['response']
