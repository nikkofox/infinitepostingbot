import random

import requests


class YADiskApi:
    def __init__(self, token):
        self.headers = {'Authorization': f'OAuth {token}'}
        self.count_image = self.get_count_img(self.headers)
        self.free_space = self.get_free_space(self.headers)
        self.image_path = ""

    def get_image(self):
        params = {'path': '/pics', 'limit': '1', 'offset': str(random.randint(0, self.count_image))}
        image = requests.get('https://cloud-api.yandex.net/v1/disk/resources',
                             headers=self.headers, params=params).json().get('_embedded').get('items')
        self.image_path = image[0].get('path')
        return image[0].get('file'), image[0].get('name'), image[0].get('path')

    def delete_image(self):
        if self.image_path:
            response = requests.delete('https://cloud-api.yandex.net/v1/disk/resources', headers=self.headers,
                                       params={'path': self.image_path})
            if 204 != response.status_code:
                raise Exception(response.json().get("error"))

    @staticmethod
    def get_free_space(headers):
        info_about_disk = requests.get('https://cloud-api.yandex.net/v1/disk/', headers=headers).json()
        free_space = int(info_about_disk['total_space']) - int(info_about_disk['used_space'])
        return free_space

    @staticmethod
    def get_count_img(headers):
        count_img = requests.get('https://cloud-api.yandex.net/v1/disk/resources', headers=headers,
                                 params={'path': '/pics', 'limit': '0'}).json()
        count_img = int(count_img['_embedded']['total']) - 1
        return count_img


def download_image(download_path):
    response = requests.get(f"{download_path}")
    return response.content
