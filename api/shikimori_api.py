import logging
import os
import sqlite3

import requests

domain = "shikimori.one"


class ShikimoriApi:
    def __init__(self, client_id, client_secret, application):
        self.db = os.path.join('db', 'shikimori.sqlite')
        self.domain = domain
        sql = open(os.path.join('db', 'shikimori.sql')).read()
        self.connect = sqlite3.connect(self.db)
        with self.connect:
            cursor = self.connect.cursor()
            cursor.executescript(sql)
            cursor.close()

        self.cursor = self.connect.cursor()
        self.client_id = client_id
        self.client_secret = client_secret
        self.headers = {'User-Agent': application}
        self.cursor.execute("SELECT access_token FROM shikimori_token")
        token, = self.cursor.fetchone()
        self.headers['Authorization'] = f'Bearer {token}'

    def get(self, api_endpoint, params=''):
        response = requests.get(f'https://{self.domain}/api/{api_endpoint}', headers=self.headers, params=params)
        expire = self.__check_token_expire(response)
        if expire:
            response = requests.get(f'https://{self.domain}/api/{api_endpoint}', headers=self.headers, params=params)
        return response

    def __check_token_expire(self, response):
        if response.status_code == 401:
            try:
                self.cursor.execute("SELECT access_token, refresh_token FROM shikimori_token")
                access_token, refresh_token = self.cursor.fetchone()
                self.headers.pop('Authorization')
                access_token = self.update_token(access_token, refresh_token)
                self.headers['Authorization'] = f'Bearer {access_token}'
                return True
            except Exception as e:
                logging.exception(e)
                raise Exception('Failed access token update')
        return False

    def update_token(self, access_token, refresh_token):
        response = requests.post(f"https://{self.domain}/oauth/token", headers=self.headers,
                                 params={'grant_type': "refresh_token", 'client_id': self.client_id,
                                         'client_secret': self.client_secret,
                                         'refresh_token': refresh_token}).json()
        self.cursor.execute("UPDATE shikimori_token "
                            "SET access_token = '%s', refresh_token = '%s' "
                            "WHERE access_token = '%s'" %
                            (response['access_token'], response['refresh_token'], access_token))
        self.connect.commit()
        return response['access_token']

    def get_image(self, original):
        response = requests.get(f"https://{self.domain}{original}", headers=self.headers)
        expire = self.__check_token_expire(response)
        if expire:
            response = requests.get(f"https://{self.domain}{original}", headers=self.headers)
        return response

    def close(self):
        self.cursor.close()
        self.connect.close()
