from datetime import datetime
from enum import Enum

import requests

GRAPHQL_URL = 'https://graphql.anilist.co'


class Season(str, Enum):
    WINTER = 'WINTER'
    SPRING = 'SPRING'
    SUMMER = 'SUMMER'
    FALL = 'FALL'


def prepare_anilist_season_anime_data(
        season: Season, year: int,
        format_: str = None, format_not: str = None,
        status: str = None,
        min_episodes: int = None,
        page: int = None
) -> dict:
    variables = {x: y for x, y in locals().items() if y is not None}
    return {'variables': variables,
            'query': """query (
                        $season: MediaSeason,
                        $year: Int,
                        $format: MediaFormat,
                        $excludeFormat: MediaFormat,
                        $status: MediaStatus,
                        $minEpisodes: Int,
                        $page: Int,
                    ){
                        Page(page: $page) {
                            pageInfo {
                                hasNextPage
                                total
                            }
                            media(
                                season: $season
                                seasonYear: $year
                                format: $format,
                                format_not: $excludeFormat,
                                status: $status,
                                episodes_greater: $minEpisodes,
                                isAdult: false,
                                type: ANIME,
                                sort: POPULARITY_DESC,
                            ) {
                                
                    id
                    idMal
                    title {
                        romaji
                        native
                        english
                    }
                    startDate {
                        year
                        month
                        day
                    }
                    endDate {
                        year
                        month
                        day
                    }
                    status
                    season
                    format
                    genres
                    synonyms
                    duration
                    popularity
                    episodes
                    source(version: 2)
                    countryOfOrigin
                    hashtag
                    averageScore
                    siteUrl
                    description
                    bannerImage
                    isAdult
                    coverImage {
                        extraLarge
                        color
                    }
                    trailer {
                        id
                        site
                        thumbnail
                    }
                    externalLinks {
                        site
                        url
                    }
                    rankings {
                        rank
                        type
                        season
                        allTime
                    }
                    studios(isMain: true) {
                        nodes {
                            id
                            name
                            siteUrl
                        }
                    }
                    relations {
                        edges {
                            relationType(version: 2)
                            node {
                                id
                                title {
                                    romaji
                                    native
                                    english
                                }
                                siteUrl
                            }
                        }
                    }
                    
                    airingSchedule(
                        notYetAired: true
                        perPage: 2
                    ) {
                        nodes {
                            episode
                            airingAt
                        }
                    }
                    
                            }
                        }
                    }"""
            }


class AnilistApi:
    def __init__(self):
        self.graphql = GRAPHQL_URL

    @staticmethod
    def calc_season(dt: datetime) -> Season:
        if datetime(day=25, month=12, year=dt.year) <= dt <= datetime(day=31, month=12, year=dt.year):
            return Season.WINTER
        elif datetime(day=1, month=1, year=dt.year) <= dt < datetime(day=25, month=3, year=dt.year):
            return Season.WINTER
        elif datetime(day=25, month=3, year=dt.year) <= dt < datetime(day=25, month=6, year=dt.year):
            return Season.SPRING
        elif datetime(day=25, month=6, year=dt.year) <= dt < datetime(day=25, month=9, year=dt.year):
            return Season.SUMMER
        elif datetime(day=25, month=9, year=dt.year) <= dt < datetime(day=25, month=12, year=dt.year):
            return Season.FALL

    def post(self, query: str, variables: dict = None):
        response = requests.post(self.graphql, json={'query': query, 'variables': variables or {}})
        return response
