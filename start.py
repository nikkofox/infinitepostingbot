#!/usr/bin/env python

import logging

from scheduler import Scheduler
from scheduler import background_tasks

logging.basicConfig(level=logging.DEBUG, format=u"%(levelname)-8s[%(asctime)s] | %(message)s",
                    datefmt='%m/%d/%Y %H:%M:%S', filemode='w', filename="out.log")

scheduler = Scheduler()
scheduler.set_interval()

back_tasks = background_tasks()
back_tasks.start()
scheduler.start()
