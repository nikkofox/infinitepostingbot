import os
import re
import sqlite3

special_titles = {'vocaloid', 'overwatch', 'kantai collection', 'pokemon adventures', 're zero', 'haikyuu', 'touhou',
                  'pokemon', 'idolmaster', 'the idolm@ster', 'closers', 'league of legends', 'love live', 'fate',
                  'new game', 'nier automata', 'final fantasy', 'hakoniwa logic', 'hyperdimension neptunia',
                  'otome ga tsumugu koi no canvas', 'your name', 'aikatsu', 'warcraft', 'deemo', "the witch's house",
                  'to aru majutsu no index', 'shironeko project', 'kagerou project', "girls frontline", 'disney',
                  'angels of death', 'bleach', 'tokyo ghoul', 'cuphead', 'ragnarok online', 'xenosaga', 'durarara',
                  'undertale', 'sound horizon', "jojo", 'bloodborne', 'dark souls', 'monogatari', 'gray garden',
                  'the gray garden', 'neko-nin exheart', 'Nekonin exHeart', 'harry potter', 'a song of ice and fire',
                  'elsword', 'blue reflection', 'rwby', 'super dangan ronpa 2', 'super dangan ronpa', 'voiceroid',
                  'new danganronpa', 'new danganronpa v3', 'show by rock', 'fairy tales', 'azur lane', 'yume nikki',
                  'gray garden', 'the gray garden', 'doki doki literature club', 'sinoalice', 'super mario bros',
                  'super mario', 'evangelion', 'miraculous ladybug', 'hololive', 'unlight', 'pixiv fantasia',
                  'dangan ronpa', 'danganronpa', 'sayonara zetsubou sensei', 'ib', "american mcgee's alice",
                  }


def parsing_image_name(image_name):
    anime_title = None
    temp = image_name.replace(' [PNG]', '').replace(' [GIF]', '').replace("_", " ")
    if 'ZC ' in temp:
        anime_title = re.search(r"ZC\s+\S+\s+-\s+(?P<title>.*?)\s*(?:-\s|~\s|\.[\w]+$)", temp)
    elif 'e-shuushuu' in temp:
        anime_title = re.search(r"e-shuush[.\w]+\s-\s\S+\s-\s(?P<title>.*?)\s*(?:-\s|~\s|\.[\w]+$)", temp)
    elif 'chan.sankakucomplex' in temp:
        anime_title = re.search(r"chan\.sankaku\S*\s+-\s+\S+\s+-\s+(?P<title>.*?)\s*(?:-\s|~\s|\.[\w]+$)", temp)
    elif any((i in temp for i in ('safebooru', 'gelbooru', 'konachan', 'yande.re', 'anime-pictures', 'zerochan'))):
        anime_title = re.search(
            r"(\S*zerochan|anime-pictures|safebooru|gelbooru|konachan|yande\.re)"
            r"\S*\s+-\s+\S+\s+-\s+.*?(?P<title>.*?)\s*(?:-\s|~\s|\.[\w]+$)",
            temp)

    return '' if not anime_title else anime_title.group('title')


def find_title_in_db(title):
    connect = sqlite3.connect(os.path.join('db', 'anime_titles.sqlite'))
    cursor = connect.cursor()
    if title not in {'', 'original', 'high resolution', 'misc'}:
        ' '.join(title.split())
        for i in special_titles:
            if (i.lower() in title.lower() and len(i) >= 6) or title.lower().startswith(i.lower()):
                return i
        if title.lower() in special_titles:
            return title
        else:
            cursor.execute(
                "SELECT * FROM anime_tables "
                "WHERE en LIKE ? OR xjat LIKE ? OR xjat_syn LIKE ? OR en_short LIKE ?", (
                    f"{title}%", f"{title}%", f"{title}%", f"{title}%"
                ))
            title = cursor.fetchone()
            connect.close()
            if title:
                return title[1]
            else:
                return ''
    else:
        return ''
