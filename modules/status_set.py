import configparser
import os

from api.vk_api import status_update
from api.yadisk_api import YADiskApi

config = configparser.ConfigParser()
config.read('config.ini')


def load_info(token):
    st = os.statvfs('/')
    try:
        ya_disk = YADiskApi(token)
        ya_disk_free_space = round(ya_disk.free_space / (1024 ** 3), 2)
        ya_disk_count_img = ya_disk.count_image
    except Exception as e:
        ya_disk_free_space = 0
        ya_disk_count_img = 0
    local_count_img = len(os.listdir(config["BotSettings"].get("path")))
    local_space = round(((st.f_bavail * st.f_frsize) / (1024 ** 3)), 2)
    return local_count_img, ya_disk_count_img, f'{local_space}GB', f'{ya_disk_free_space}GB'


def text_status_update(vk_token, ya_token, group_id):
    local_count, ya_count, local_space, ya_space = load_info(ya_token)
    text = f"VDS: {local_count} / {local_space} | YaD: {ya_count} / {ya_space}"
    result = status_update(vk_token, group_id, text)
    if result == 1:
        return True
    else:
        return False
