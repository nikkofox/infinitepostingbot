import configparser
from datetime import datetime

from api import vk_api
from api.anilist_api import AnilistApi, prepare_anilist_season_anime_data

config = configparser.ConfigParser()
config.read('config.ini')
ANICHART_LINK = 'https://anichart.net/'


class AnilistSeasonPoster:
    def __init__(self):
        self.anilist = AnilistApi()

    def post(self):
        group_name = config['VK'].get("group_name")
        vk_token = config['VK'].get("token")
        group_id = config['VK'].get("group_id")

        message = f'#популярно@{group_name}\nТоп популярных #аниме сезона:\n\n'
        for i, anime in enumerate(self._get_season_top()[:30], 1):
            title = anime['title']
            message += f'{i}. {title.get("romaji")} | {title.get("english")}' \
                       f'\nЖанры: {", ".join(anime["genres"])}' \
                       f'\nРейтинг: {anime["averageScore"]}%' \
                       f'\nПодробнее: {anime["siteUrl"]}\n\n'

        message += ANICHART_LINK
        posting = vk_api.Posting(vk_token, group_id, message=message, attachments=[])
        post_id = posting.create_post()
        if post_id:
            result = vk_api.wall_pin(vk_token, post_id, -int(group_id))
            if result:
                return True
        return False

    def _get_season_top(self):
        dt = datetime.today()
        return self.anilist.post(
            **prepare_anilist_season_anime_data(
                season=self.anilist.calc_season(dt),
                year=dt.year)
        ).json()['data']['Page']['media']
