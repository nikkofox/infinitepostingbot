import configparser
import os
import sqlite3
import time
from datetime import datetime, timedelta
from typing import Optional

from api import vk_api
from api.shikimori_api import ShikimoriApi

config = configparser.ConfigParser()
config.read('config.ini')


class SeasonPoster:
    def __init__(self):
        self.db = os.path.join('db', 'shikimori.sqlite')
        sql = open(os.path.join('db', 'shikimori.sql')).read()
        self.connect = sqlite3.connect(self.db)
        with self.connect:
            cursor = self.connect.cursor()
            cursor.executescript(sql)
            cursor.close()
        client_id = config['Shikimori'].get("client_id")
        client_secret = config['Shikimori'].get("client_secret")
        application = config['Shikimori'].get("application")
        self.shiki_api = ShikimoriApi(client_id, client_secret, application)

    def post(self):
        group_name = config['VK'].get("group_name")
        vk_token = config['VK'].get("token")
        group_id = config['VK'].get("group_id")

        message = f'#популярно@{group_name}\nТоп популярных #аниме сезона:\n\n'
        for i, anime in enumerate(self.__get_top_from_shiki()):
            shikimori_url = f"https://{self.shiki_api.domain}/animes/{anime.get('id')}"
            rus_anime_info = self.__get_russian_info(anime.get('id'))
            message += f'{i + 1}. {anime.get("name")} | {anime.get("russian")}' \
                       f'\nЖанры: {rus_anime_info.get("genres")}' \
                       f'\nРейтинг: {anime.get("score")}' \
                       f'\nПодробнее: {shikimori_url}\n\n'
            time.sleep(0.5)

        posting = vk_api.Posting(vk_token, group_id, message=message, attachments=[])
        post_id = posting.create_post()
        self.shiki_api.close()
        if post_id:
            result = vk_api.wall_pin(vk_token, post_id, -int(group_id))
            if result:
                return True
        return False

    def __get_top_from_shiki(self):
        dt = datetime.today()
        return self.shiki_api.get(f'animes',
                                  {'order': 'popularity', 'season': self.calc_season(dt), 'limit': 30}).json()

    def __get_russian_info(self, anime_id):
        anime = self.shiki_api.get(f'animes/{anime_id}').json()
        genres = [x.get('russian') for x in anime['genres']]
        title = anime.get('russian')
        return {'title': title, 'genres': ', '.join(genres)}

    @staticmethod
    def calc_season(dt: datetime) -> Optional[str]:
        if datetime(day=25, month=12, year=dt.year) <= dt <= datetime(day=31, month=12, year=dt.year):
            return f'winter_{(dt+timedelta(weeks=10)).year}'
        elif datetime(day=1, month=1, year=dt.year) <= dt < datetime(day=25, month=3, year=dt.year):
            return f'winter_{dt.year}'
        elif datetime(day=25, month=3, year=dt.year) <= dt < datetime(day=25, month=6, year=dt.year):
            return f'spring_{dt.year}'
        elif datetime(day=25, month=6, year=dt.year) <= dt < datetime(day=25, month=9, year=dt.year):
            return f'summer_{dt.year}'
        elif datetime(day=25, month=9, year=dt.year) <= dt < datetime(day=25, month=12, year=dt.year):
            return f'fall_{dt.year}'
