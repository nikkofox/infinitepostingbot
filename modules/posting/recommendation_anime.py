import configparser
import os
import random
import sqlite3
import time

from PIL import Image
from bs4 import BeautifulSoup

from api import vk_api
from api.shikimori_api import ShikimoriApi

Image.MAX_IMAGE_PIXELS = None
config = configparser.ConfigParser()
config.read('config.ini')


class RecommendationPoster:
    def __init__(self):
        self.db = os.path.join('db', 'shikimori.sqlite')
        sql = open(os.path.join('db', 'shikimori.sql')).read()
        self.connect = sqlite3.connect(self.db)
        with self.connect:
            cursor = self.connect.cursor()
            cursor.executescript(sql)
            cursor.close()
        client_id = config['Shikimori'].get("client_id")
        client_secret = config['Shikimori'].get("client_secret")
        application = config['Shikimori'].get("application")
        self.shiki_api = ShikimoriApi(client_id, client_secret, application)

    def post(self):
        ani_id = self.__search_anime(self.__get_page())
        message, attachments = self.__get_anime(ani_id)
        vk_token = config['VK'].get("token")
        group_id = config['VK'].get("group_id")
        posting = vk_api.Posting(vk_token, group_id, message=message, attachments=attachments)
        post_id = posting.create_post()
        self.shiki_api.close()
        return True if post_id else False

    def __search_anime(self, page):
        cursor = self.connect.cursor()
        anime_id = False
        while True:
            animes = self.shiki_api.get('animes', {'score': '7', 'order': 'popularity', 'status': 'released',
                                                   'kind': 'tv,ona,movie', 'limit': '10', 'page': page}).json()
            if 'error' in animes and animes['error'] != 'invalid_token':
                raise Exception('Error in get animes')
            else:
                for i in animes:
                    cursor.execute("SELECT anime_id FROM recommendation_page WHERE anime_id = '%s'" % i['id'])
                    ani_id = cursor.fetchone()
                    if not ani_id:
                        cursor.execute(
                            "INSERT INTO recommendation_page (anime_id, page) "
                            "VALUES ('%s', '%s')" % (int(i['id']), int(page)))
                        self.connect.commit()
                        anime_id = i['id']
                        break
                if anime_id:
                    break
            time.sleep(1.5)
            page = random.randint(1, 85)
        cursor.close()
        return anime_id

    def __get_page(self):
        with self.connect:
            cursor = self.connect.cursor()
            cursor.execute("SELECT page FROM recommendation_page WHERE id = (SELECT max(id) FROM recommendation_page)")
            page = cursor.fetchone()
        return random.randint(1, 85) if not page else page[0]

    def __get_anime(self, ani_id):
        anime_info = self.shiki_api.get(f'animes/{ani_id}').json()
        anime_screenshots = self.shiki_api.get(f'animes/{ani_id}/screenshots').json()
        attachments = []
        album = config['VK'].get("recommendation_album")
        group_name = config['VK'].get("group_name")
        for video in anime_info['videos']:
            if 'youtu' in video['url'] or 'vk.com' in video['url']:
                attachments.append(vk_api.Attachment('VIDEO', video['url'], 'video', album))
            else:
                continue

        anime_screenshots = anime_screenshots if len(anime_screenshots) <= 6 else random.choices(anime_screenshots, k=6)
        for i in anime_screenshots:
            time.sleep(1)
            bytes_image = self.shiki_api.get_image(i['original']).content
            attachments.append(vk_api.Attachment(f'image{int(time.time())}.jpg', bytes_image, 'image'))

        message = f"{anime_info['russian']} / {anime_info['name']}\n\nТип: " \
            f"{anime_info['kind'].replace('movie', 'Фильм').replace('tv', 'TV Сериал').replace('ona', 'ONA')}" \
            f"\nЭпизоды: {anime_info['episodes']}\n"

        if anime_info['released_on']:
            temp = anime_info['aired_on'].split('-')
            temp2 = anime_info['released_on'].split('-')
            aired = time.strftime('%d %b %Y', (int(temp[0]), int(temp[1]), int(temp[2]), 0, 0, 0, 0, 0, 0))
            released = time.strftime('%d %b %Y', (int(temp2[0]), int(temp2[1]), int(temp2[2]), 0, 0, 0, 0, 0, 0))
            message = message + f"Дата выпуска: с {aired} г. по {released} г.\n"
        else:
            temp = anime_info['aired_on'].split('-')
            aired = time.strftime('%d %b %Y', (int(temp[0]), int(temp[1]), int(temp[2]), 0, 0, 0, 0, 0, 0))
            message = message + f"Дата выпуска: {aired} г.\n"

        genres = map(lambda x: f"#{x['russian'].replace(' ', '_')}@{group_name}", anime_info['genres'])
        temp = '\n'.join(genres)
        message = f"{message}Жанры:\n{temp}\n" \
            f"Рейтинг: {anime_info['score']}\n" \
            f"Студия: {anime_info['studios'][0]['name']}\n"

        anime_roles = self.shiki_api.get(f'animes/{ani_id}/roles').json()
        authors = []
        directors = []
        try:
            for i in anime_roles:
                if 'Original Creator' in i['roles']:
                    if i['person']['russian']:
                        authors.append(i['person']['russian'])
                    else:
                        authors.append(i['person']['name'])
                elif 'Director' in i['roles']:
                    if i['person']['russian']:
                        directors.append(i['person']['russian'])
                    else:
                        directors.append(i['person']['name'])
        except:
            pass

        if authors:
            temp_authors = 'Авторы оригинала:' if len(authors) > 1 else 'Автор оригинала:'
            message += f"{temp_authors} {', '.join(authors)}\n"
        if directors:
            temp_directors = 'Режиссёры:' if len(directors) > 1 else 'Режиссёр:'
            message += f"{temp_directors} {', '.join(directors)}\n"
        # description = re.sub(r'\[.*?\]', '', anime_info['description'].replace('[[', '').replace(']]', ''))
        soup = BeautifulSoup(anime_info['description_html']
                             .replace('<div class="b-prgrph">', '\n').replace('<br>', '\n'), 'lxml')
        try:
            # description = '\n'.join([x.text for x in soup.find_all(class_='b-prgrph')])
            description = soup.get_text()
        except:
            description = ''
        message = f"{message}\n{description}" \
            f"\n\nShikimori: https://{self.shiki_api.domain}{anime_info['url']}" \
            f"\nMyAnimeList: https://myanimelist.net/anime/{ani_id}"
        return f'#рекомендации@{group_name} #anime #аниме\n\n{message}', attachments
