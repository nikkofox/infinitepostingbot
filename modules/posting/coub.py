import configparser
import os
import sqlite3

import requests

from api import vk_api

config = configparser.ConfigParser()
config.read('config.ini')


class CoubPoster:
    def __init__(self):
        self.db = os.path.join('db', 'coub.sqlite')
        sql = open(os.path.join('db', 'coub.sql')).read()
        connect = sqlite3.connect(self.db)
        with connect:
            cursor = connect.cursor()
            cursor.execute(sql)
            cursor.close()

    def post(self):
        coub_url = self.__search_coub()
        if coub_url:
            vk_token = config['VK'].get("token")
            group_id = config['VK'].get("group_id")
            group_name = config['VK'].get("group_name")
            coub_album = config['VK'].get("coub_album")
            message = f'#coub@{group_name} #anime #аниме'
            attachments = vk_api.Attachment('coub', coub_url, 'video', coub_album)
            posting = vk_api.Posting(vk_token, group_id, message=message, attachments=[attachments])
            posting.create_post()
            return True
        else:
            return False

    def __update_db(self, cursor, connect):
        cursor.execute("DELETE FROM coub WHERE id <= 80")
        connect.commit()
        cursor.execute("SELECT * FROM coub")
        temp_data = cursor.fetchall()
        for i in range(len(temp_data)):
            cursor.execute("UPDATE coub SET id = '%s' WHERE id = '%s'" % (i, temp_data[i][0]))
            connect.commit()

    def __search_coub(self):
        newest_popular = requests.get('http://coub.com/api/v2/timeline/tag/anime?order_by=newest_popular').json()
        connect = sqlite3.connect(self.db)
        cursor = connect.cursor()
        cursor.execute("SELECT COUNT(*) FROM coub")
        coub_count = cursor.fetchone()[0]
        coub_url = ''
        if coub_count > 101:
            self.__update_db(cursor, connect)
            cursor.execute("SELECT COUNT(*) FROM coub")
            coub_count = cursor.fetchone()
        for coub in newest_popular['coubs']:
            cursor.execute(
                "SELECT id FROM coub WHERE permalink = '%s'" % coub['permalink'])
            coub_from_db = cursor.fetchone()
            if not coub_from_db:
                cursor.execute("INSERT INTO coub (id, permalink) VALUES ('%s', '%s')" % (
                    coub_count + 1, coub['permalink']))
                connect.commit()
                coub_url = f"http://coub.com/view/{coub['permalink']}"
                break
            else:
                continue
        connect.close()
        return coub_url
