import gzip
import logging
import os
import re
import sqlite3
import requests

from lxml import etree


class UpdateAnimeTitles:
    def __init__(self):
        self.download_path = os.path.join('modules', 'extra', 'download')
        if not os.path.exists(self.download_path):
            os.makedirs(self.download_path)
        self.db = os.path.join('db', 'anime_titles.sqlite')
        self.archive_path = os.path.join(self.download_path, 'anime_titles.gz')
        self.xml_path = os.path.join(self.download_path, 'anime_titles.xml')
        self.__create_db()

    def __download_archive(self):
        try:
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                              '(KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'}
            archive = requests.get('http://anidb.net/api/anime-titles.xml.gz', headers=headers).content
            with open(self.archive_path, 'wb') as file:
                file.write(archive)
            return True
        except Exception as e:
            logging.error(f'`download anime archive` EXCEPTION: {e}')
            return False

    def __create_db(self):
        sql = open(os.path.join('db', 'anime_titles.sql')).read()
        connect = sqlite3.connect(self.db)  # подключаемся к бд
        with connect:
            cursor = connect.cursor()
            cursor.execute(sql)  # создаем таблицу в базе данных, если ее нет

    def __extract_archive(self):
        try:
            open_archive = gzip.open(self.archive_path).read()
            with open(self.xml_path, 'wb') as file:
                file.write(open_archive)
            os.remove(self.archive_path)
            return True
        except Exception as e:
            logging.error(f'`extract anime archive` EXCEPTION: {e}')
            return False

    def update_db(self):
        if self.__download_archive():
            if self.__extract_archive():
                xml = open(self.xml_path, 'rb').read()
                root = etree.fromstring(xml)
                anime = root.getchildren()
                connect = sqlite3.connect(self.db)
                cursor = connect.cursor()
                cursor.execute("DELETE FROM anime_tables")
                ficha = '{http://www.w3.org/XML/1998/namespace}lang'
                for j in range(len(anime)):
                    en_official, x_jat_syn, x_jat, en_short = '', '', '', ''
                    for i in range(len(anime[j])):
                        if anime[j][i].attrib['type'] == 'official' and anime[j][i].attrib[ficha] == 'en':
                            en_official = re.sub(r':.*', '', anime[j][i].text)
                            en_official = re.sub(r' [0-9]$', '', en_official)
                            en_official = re.sub(r'[\W\s][0-9]{4}[\W\s]', '', en_official).strip()
                        elif anime[j][i].attrib['type'] == 'main' and anime[j][i].attrib[ficha] == 'x-jat':
                            x_jat = re.sub(r':.*', '', anime[j][i].text)
                            x_jat = re.sub(r' [0-9]$', '', x_jat)
                            x_jat = re.sub(r'[\W\s][0-9]{4}[\W\s]', '', x_jat).strip()
                        elif anime[j][i].attrib['type'] == 'syn' and anime[j][i].attrib[ficha] == 'x-jat':
                            x_jat_syn = re.sub(r':.*', '', anime[j][i].text)
                            x_jat_syn = re.sub(r' [0-9]$', '', x_jat_syn)
                            x_jat_syn = re.sub(r'[\W\s][0-9]{4}[\W\s]', '', x_jat_syn).strip()
                        elif anime[j][i].attrib['type'] == 'short' and anime[j][i].attrib[ficha] == 'en':
                            en_short = re.sub(r':.*', '', anime[j][i].text)
                            en_short = re.sub(r' [0-9]$', '', en_short)
                            en_short = re.sub(r'[\W\s][0-9]{4}[\W\s]', '', en_short).strip()
                    cursor.execute(
                        "INSERT INTO anime_tables (id, xjat, xjat_syn, en, en_short) "
                        "VALUES ('%s', '%s', '%s', '%s', '%s')" % (
                            int(anime[j].attrib['aid']), str(x_jat), str(x_jat_syn), str(en_official), str(en_short)
                        ))
                    connect.commit()
                connect.close()
                os.remove(self.xml_path)
