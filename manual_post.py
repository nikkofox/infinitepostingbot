#!/usr/bin/env python

import argparse
import logging

from scheduler import (season_post, image_post, multiple_image_post,
                       recommendation_post, update_status, coub_post, gif_post)

logging.basicConfig(
    level=logging.DEBUG, format=u"%(levelname)-8s[%(asctime)s] | %(message)s", datefmt='%m/%d/%Y %H:%M:%S'
)

parser = argparse.ArgumentParser(description='Оболочка для ручного запуска и создания постов в вк')
parser.add_argument('-s', '--status', type=bool, help='обновить ли статус в группе', default=False, required=False)
subparsers = parser.add_subparsers(dest='command', help='Список команд', required=True)
subparsers.add_parser('season', help='топ аниме сезона').set_defaults(func=season_post)
subparsers.add_parser('recom', help='рекомендация годного аниме').set_defaults(func=recommendation_post)
subparsers.add_parser('image', help='пост одной картинки').set_defaults(func=image_post)
subparsers.add_parser('mimage', help='пост из нескольких картинок').set_defaults(func=multiple_image_post)
subparsers.add_parser('coub', help='пост аниме коуба').set_defaults(func=coub_post)
subparsers.add_parser('gif', help='пост аниме гифки').set_defaults(func=gif_post)
args = parser.parse_args()

args.func()

if args.status:
    update_status()
